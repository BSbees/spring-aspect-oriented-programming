package com.luv2code.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.luv2code.aopdemo.dao.AccountDAO;
import com.luv2code.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String [] args) {
		//read Spring config class
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		//getting bean
		AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);
		
		//call business method
		System.out.println("Call accountDAO method");
		Account account = new Account("test", "test");
		accountDAO.addAccount(account, true);
		
		//getters and setrers
		accountDAO.setLevel("test");
		accountDAO.setName("name");
		
		String name = accountDAO.getName();
		String level = accountDAO.getLevel();
		
//		membershipDAO.goSleep();
		//closing context
		context.close();
	}
}