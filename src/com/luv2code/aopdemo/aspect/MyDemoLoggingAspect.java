package com.luv2code.aopdemo.aspect;

import java.util.List;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.luv2code.aopdemo.Account;

@Aspect
@Component
@Order(3)
public class MyDemoLoggingAspect {
	
	private static Logger logger = Logger.getLogger(MyDemoLoggingAspect.class.getName());
	
	@Around("execution(* com.luv2code.aopdemo.service.*.getFortune(..))")
	public Object aroundGetFortune(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
		logger.info("Procced to execute: " + proceedingJoinPoint.getSignature().toShortString());
		
		//time before calling method
		long begin = System.currentTimeMillis();
		Object result = proceedingJoinPoint.proceed();
		
		//time after calling mehtod
		long end = System.currentTimeMillis();
		
		logger.info("Duration: " + ((end-begin)/1000.0));
		
		return result;
	}
	
	@After("execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))")
	public void afterFindAccountsAdvise(JoinPoint joinPoint){
		logger.info("Method " + joinPoint.getSignature().toShortString());
		
	}
	
	@AfterThrowing(pointcut="execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",
			throwing="exc")
	public void afterThrowingFindAccountsAdvise(JoinPoint joinPoint, Exception exc){
		logger.info("Exception thrown at " + joinPoint.getSignature().toShortString());
		logger.info("exception: " + exc);
	}
	
	@Before("com.luv2code.aopdemo.aspect.AopExpressions.forDAOPackageNoGetterSetter()")
	public void beforeAddAccountAdvice(JoinPoint joinPoint){
		logger.info("\n====>Before addAccount() advice action<====");
		
		//printing method signature
		MethodSignature methodSignature= (MethodSignature) joinPoint.getSignature();
		logger.info("Method signature: " + methodSignature);
		
		//printing passed args
		Object [] args = joinPoint.getArgs();
		for (Object o : args){
			logger.info(o.toString());
			if (o instanceof Account){
				Account account = (Account) o;
				logger.info("Account name: " + account.getName());
				logger.info("Account level: " + account.getLevel());
			}
		}
	}
	
	@AfterReturning(pointcut="execution(* com.luv2code.aopdemo.dao.AccountDAO.findAccounts(..))",
			returning="accounts")
	public void afterReturningFindAccountsService(JoinPoint joinPoint, List<Account> accounts){
		String method = joinPoint.getSignature().toShortString();
		logger.info("\n====>Execute @AfterReturning for metod: " + method);
		
		//post-processing returned data
		convertAccountNamesToUpperCase(accounts);
		//Slogger.info(accounts);
	}

	private void convertAccountNamesToUpperCase(List<Account> accounts) {

		for(Account account: accounts){
			account.setName(account.getName().toUpperCase());
		}
	}
}
