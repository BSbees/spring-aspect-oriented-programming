package com.luv2code.aopdemo.dao;

import org.springframework.stereotype.Component;

@Component
public class MembershipDAO {

	public boolean addSillyMember(){
		System.out.println(getClass() + "MembershipDAO addAccount() function call");
		return false;
	}
	
	public boolean goSleep(){
		System.out.println(getClass() + "snoozing");
		return false;
	}
}
