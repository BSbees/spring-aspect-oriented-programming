package com.luv2code.aopdemo.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.luv2code.aopdemo.Account;

@Component
public class AccountDAO {
	
	private String name;
	private String level;
	
	public List<Account> findAccounts(boolean tripWire){
		if(tripWire){
			throw new RuntimeException("No soup for you!!!");
		}
		List<Account> accounts = new ArrayList<Account>();
		accounts.add(new Account("John", "Silver"));
		accounts.add(new Account("Madhu", "Platinium"));
		accounts.add(new Account("Luca", "Gold"));
		return accounts;
	}
	
	public String getName() {
		System.out.println("getter " + getClass());
		return name;
	}

	public void setName(String name) {
		System.out.println("setter " + getClass());
		this.name = name;
	}

	public String getLevel() {
		System.out.println("getter " + getClass());
		return level;
	}

	public void setLevel(String level) {
		System.out.println("setter " + getClass());
		this.level = level;
	}

	public void addAccount(Account account, boolean vip){
		System.out.println(getClass() + "DB work will be here");
	}
	
	public boolean doWork(){
		System.out.println(getClass() + "Doing hard work");
		return false;
	}
}
